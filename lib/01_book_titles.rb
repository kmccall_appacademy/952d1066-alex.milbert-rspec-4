class Book
  attr_reader :title

  public
  def title=(name)
    @title = titleize(name)
  end
  private
  def titleize(title)
    results = []
    helper_words = ['and','the','or','a','in','of','on','an']
    title_word = title.split(' ')
    title_word.each_index do |idx|
      if idx > 0 && helper_words.include?(title_word[idx])
        results << title_word[idx]
      else
        results << title_word[idx].capitalize
      end
    end
  results.join(' ')
  end
end
